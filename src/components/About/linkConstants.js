const linkTypes = {
  FACEBOOK: "facebook",
  TWITTER: "twitter",
  LINKEDIN: "linkedin",
  GITHUB: "github",
  BITBUCKET: "bitbucket"
};

export default linkTypes;
