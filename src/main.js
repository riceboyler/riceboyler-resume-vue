// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import Gravatar from "vue-gravatar";
import App from "./App";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.component("v-gravatar", Gravatar);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  components: { App },
  template: "<App/>"
});
